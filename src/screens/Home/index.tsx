import React from 'react';
import { MenuButton } from '@components';
import * as S from './styles';

const Home = () => {
  return (
    <S.Container>
      <S.Header>
        <S.GreetingWrapper>
          <S.GreetingTitle>Seja Bem-Vindo</S.GreetingTitle>
          <S.GreetingSubtitle>O que deseja pra hoje?</S.GreetingSubtitle>
        </S.GreetingWrapper>

        <MenuButton />
      </S.Header>
    </S.Container>
  );
};

export default Home;
