import styled from 'styled-components/native';

export const Container = styled.View({
  flex: 1,
  backgroundColor: '#F9F9FB',
});

export const Header = styled.View({
  marginRight: 20,
  marginLeft: 20,

  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
});

export const GreetingWrapper = styled.View({
  flexDirection: 'column',
});

export const GreetingTitle = styled.Text({
  fontSize: 24,
  color: '#1b1b1b',
});

export const GreetingSubtitle = styled.Text({
  fontSize: 16,
  color: '#979797',
});
