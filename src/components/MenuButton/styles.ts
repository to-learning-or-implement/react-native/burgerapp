import styled from 'styled-components/native';

export const Container = styled.View({
  width: 24,
  height: 24,

  padding: 2,
});

export const StraightLine = styled.View({
  width: '100%',
  height: 2,

  backgroundColor: 'red',
  marginBottom: 5,
});
