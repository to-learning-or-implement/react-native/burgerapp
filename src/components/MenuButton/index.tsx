import React from 'react';
import * as S from './styles';

const MenuButton = () => {
  return (
    <S.Container>
      <S.StraightLine />
      <S.StraightLine />
      <S.StraightLine />
    </S.Container>
  );
};

export default MenuButton;
